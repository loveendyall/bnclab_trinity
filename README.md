# BNClab back-end & front-end instructions

BNC is split into a back-end Node.js app and a front-end Angular v2 app.

## Node.js back-end
Node.js receives a search string entered into the input search box.

It then processes the search string into a query that targets word objects in the BNC database.
Search queries can target the words themselves, or the meta-tags associated with a word token e.g. part-of-speech or semantic fields.

Multiple adjacent tokens in the search query mean a sequence.
If the ```OR``` operator is used, then the operands around the ```OR``` apply to a single token, e.g. ```NOUN OR dog``` would find all tokens marked as nouns and combine this with all tokens equaling ```dog```. Duplicates aren't found.

Since the BNC database is so huge, and sometimes multiple search queries can be issued simultaneously,
the Node.js RestApi router (master) receives search requests, but passes these request details over to multiple child Node.js instances which each process a unique subset of the entire BNC database.

The child Node.js instances then return the results to the master, which transmits in chunks the concordances back to the browser via websockets (Socket.io)

Browsers can be on mobile devices or older desktops so a random sample of the resulting concordances are returned to the browser client to prevent the user's computer from crashing.

However, the statistics and charts are across __ALL__ the results, meaning valid conclusions can still be extracted.

To spin-up the backend services, you need to first install mongoDB, convert the BNC spoken word XML files into JSON format, then ingest the JSON files into MongoDB.

This should already be set-up on [leech.lancs.ac.uk](leech.lancs.ac.uk), but if you need to import more or set it up again here are the instructions.
### MongoDB setup
- Install [MongoDB](https://docs.mongodb.com/manual/installation/)
- Create user ```bncNode``` with password ```change_me``` (```see nodenc-master/dist/config.js```)
- Create database ```BNC```
- Create collections ```sentences``` & ```speakers```
- Using [csvtojson](https://www.npmjs.com/package/csvtojson),
execute ```./node_modules/.bin/csvtojson {csvfile.csv} > {jsonfile.json}``` to convert a .CSV file to JSON format
- Execute
```mongoimport -u {username} -p {password} --db {database} --file {jsonfile.json} --collection {collection name} --jsonArray```

    - **NOTE** the ```--jsonArray``` flag is if your JSON file contains an array of JSON objects. This will work for the ```speakers.csv```, but the BNC files are individual documents, so don't import these using ```--jsonArray``` (the JSON file is a single JSON object / document, and not an array).
    - you might want to use a bash script to import the dozens of BNC json files in a single command e.g.
    ``` for f in /../../BNC_JSON_data_directory; do mongoimport -u {username} -p {password} --db {database} --file $f --collection {collection name}; done;```

### Node.js
#### Execution
To run the back-end Node.js app, execute:

1. ```cd /../nodenc-master/```
2. ```screen -L /srv/scratch/../log_output.txt node --max-old-space-size=32000 --expose-gc ./dist/server.js 5```

```screen``` ensures the app runs in the background, otherwise when you exit the ```ssh``` into leech.lancs.ac.uk, it will also terminate any scripts you are executing.

The ```-L``` outputs the print commands from Node.js into a log file to make debugging easier if something goes wrong later.

```--max-old-space-size=32000``` flag makes sure Node.js can access enough RAM to do its processing. The BNC database can be over 4GB so this is important if users need quick response times for queries with such a large DB.
```--expose-gc``` allows you to programmatically issue a garbage collection cycle in Node.js to reduce memory footprint.
The ```5``` in the command is the number of child Node.js processes to initialise in parallel. So with ```5```, there should be 1 master Node.js runtime and 5 other worker nodes.

Feel free to play around with those flags if needed.

To terminate the Node.js instances, execute ```killall node``` on leech.lancs.ac.uk. Mind you, this will terminate both Trinity **and** BNClab node instances - you can use Ubuntu ```kill {process_id}``` to target a single process, instead. I advise targetting the master Node.js process, as the child workers will automatically terminate here.

#### Structure
- ```server.js``` inside ```dist/``` is the router for the Rest / Socket.io API.

- ```config.js``` is where you find all the configuration for the Node.js app. 

- ```routes/bnc.js``` is basically the 'master' Node.js functionality - having loaded the BNC database and allocated unique subsets to each child worker, it receives such queries, and passes the details over to the worker nodes.

- ```routes/processUTag.js``` are the worker nodes. They process a string query into a search query object, and build the concordance results as they evaluate each word-token in their BNC database subset.

- ```routes/quickSort_distributed.js``` **isn't used**, but was implemented to sort concordances alphabetically on the server, since it can take minutes for weaker browsers to do the same.
Since we decided to limit the number of concordances the browser would receive to small numbers, it was eventually deprecated.

## Angular v2 front-end
[Angular v2](https://v2.angular.io/docs/ts/latest/) serves the HTML, CS and Javascript functionality on the browser.

To build the BNClab front-end, execute:

1. ```cd /../bnc-frontend-master/```
2. ```npm install``` (installs all the 3rd party packages defined in ```package.json```)
3. ```./node_modules/@angular/cli/bin/ng build --env=prod --base-href /bnclab/ --deploy-url /bnclab/ --watch```

    - ```--env``` applies the production-ready config
    - ```--base-href``` automatically prepends ```/bnclab/``` to all the hyperlinks inside your HTML
    - ```--deploy-url``` does the same for the assets (images, javascript files etc..)
    
**NOTE** make sure ```angular-cli.json``` looks like this:
```
{ ...
  "apps": [
    {
      "root": "src",
      "outDir": "/var/www/corpora-site/bnclab",
  ...
  }],
...
}
```

This basically says output the build to ```/var/www/corpora-site/bnclab```, which is where Apache or Nginx will serve the front-end to browsers. Otherwise, it will build to the ```./dist/``` directory.
    
## Final words
These commands apply to Trinity as well, since Trinity uses the same web stack and versions as BNC.

Any issues, feel free to contact me on [loveendyall@googlemail.com](loveendyall@googlemail.com)